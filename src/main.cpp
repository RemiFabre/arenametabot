#include <stdlib.h>
#include <math.h>
#include "buzzer.h"
#include "distance.h"
#include <wirish/wirish.h>
#include <terminal.h>
#include <main.h>

TERMINAL_PARAMETER_INT(t, "Variable t", 0);

TERMINAL_COMMAND(hello, "Prints hello world")
{
  terminal_io()->println("Hello world");
}

/**
 * Setup function
 */
void setup()
{
  disableDebugPorts();
  
  //afio_remap(AFIO_REMAP_USART1);
  //gpio_set_mode(GPIOB, 6, GPIO_AF_OUTPUT_PP);
  //gpio_set_mode(GPIOB, 7, GPIO_INPUT_FLOATING);
  
  pinMode(BOARD_LED_PIN, OUTPUT);

  distance_init();
  
  terminal_init(&SerialUSB);
  t = 123;
}

/**
 * Loop function
 */
void loop()
{
  terminal_tick();
}
