1) Obstacle
h = 10 cm
largeur = 9cm

Params : height -130, altitude 23, crab -15, legradius 132, freq 2.78

2) Super crab
h = 12 (conseilé 14)
largeur = min 13 (20 conseillé)

Params : height -67, altitude 20, crab -22, legradius 115, freq 2.99

3) Barrière au sol
h= 2cm

Params : height -78, altitude 40, crab 0, legradius 147, freq 2.0

4) Bas au sol
h = 14cm (15-16)
l = 40 cm

Params : height -65, altitude 15, crab 0, legradius 174, freq 2.34

Laser thickness ? 0.1mm
x= 49.7
y = 287.5
