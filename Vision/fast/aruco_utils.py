import time, math, random, sys, os, datetime
import csv
import numpy as np
import cv2
import cv2.aruco as aruco
from metabot import MetabotV2
import pid
import asserv
import robot

DEG2RAD = math.pi/180.0
RAD2DEG = 180.0/math.pi

def detect_aruco(frame) :
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_100)
    parameters =  aruco.DetectorParameters_create()
    
    corners, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
    frame = aruco.drawDetectedMarkers(frame, corners)
    return corners, ids

def corners2xyt(corners, ids) :
    """
    Takes a list of corners and a list of ids (sharing the same length, typically the output of detect_aruco) and outputs a dictionnary {id:[x, y, theta]}. theta is in degrees.
    """
    index = -1
    result = {}
    if (ids == None or len(ids) == 0) :
        return result
    for id in ids :
        index = index + 1
        c = corners[index][0] #wtf. Todo, check out the actual format
        id = id[0] #wtf
        tl = c[0]
        tr = c[1]
        br = c[2]
        bl = c[3]
        x = (tl[0] + br[0])/2.0
        y = (tl[1] + br[1])/2.0
        #print("id = " + str(id))
        midle_point = ((tl[0] + tr[0])/2.0, (tl[1] + tr[1])/2.0)
        theta = math.atan2(midle_point[1]-y,midle_point[0]-x)*RAD2DEG
        result[id] = [x, y, theta]
    return result

def init_asserv() :
    max_d = 140 # mm/s
    max_a = 30 # deg/s
    PID_d = pid.PID(P=3.0, I=0.1, D=0.0, Derivator=0, Integrator=0, Integrator_max=max_d)
    PID_a = pid.PID(P=2.0, I=0.1, D=0.0, Derivator=0, Integrator=0, Integrator_max=max_a)
    ass = asserv.Asserv(PID_d, PID_a, max_d, max_a)

    return ass
