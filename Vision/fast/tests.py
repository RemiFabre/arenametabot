import time, math, random, sys, os, datetime
import csv
import numpy as np
import cv2
import cv2.aruco as aruco
from metabot import MetabotV2
import pid
import asserv
import robot

'''
ROBOT_IDS = [
    ("White", 3, (255, 255, 255)),
    ("Violet", 6, (211, 0, 148)),
    ("MajinBuu", 9, (147,20,255))]
'''
'''
ROBOT_IDS = [
    ("Jaune", 10, (0, 255, 255)),
    ("Vert", 11, (0, 255, 0)),
    ("Jaime", 12, (0, 69, 255)),
    ("White", 3, (255, 255, 255)),
    ("Violet", 6, (211, 0, 148)),
    ("MajinBuu", 9, (147,20,255))]
'''
ROBOT_IDS = [
    ("White", 3, (255, 255, 255)),
    ("Violet", 6, (211, 0, 148)),
    ("MajinBuu", 9, (147,20,255)),
    ("Vert", 11, (0, 255, 0)),
    ("Marker", 1, (0, 0, 0))]

#WHITE_ID = 3
#VIOLET_ID = 6
#MAJINBUU_ID = 9
CAM_ID = 0
DEG2RAD = math.pi/180.0
RAD2DEG = 180.0/math.pi


X = 0
Y = 0

CLICKED = False
R_CLICKED = False
now = datetime.datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
WITH_ROBOT = Flse

def save_list_of_pixels(list_of_pixels, color_name) :
    global now
    # Erasing and rewrinting everything.
    filename = color_name + now + ".csv"

    with open(filename, 'w') as csvfile:
        str_delimiter = " "
        data_writer = csv.writer(csvfile, delimiter=str_delimiter)
        for p in list_of_pixels :
            data_writer.writerow(p)

def load_list_of_pixels(filename) :
    try :
        with open(filename, 'r') as csvfile:
            rows = csv.reader(csvfile, delimiter=' ', quotechar='|')
            list_of_pixels = []
            for r in rows :
                lsit_of_pixels.append(r)
            return list_of_pixels
    except Exception :
        print("Failed to read : '", filename, "'")
        return


def get_boundaries_for_color(filename, percentage) :
    load_list_of_pixels(filename)

    
def mouse_action(event, x, y, flags, param):
    global X, Y, CLICKED, R_CLICKED
    if event == cv2.EVENT_LBUTTONDOWN:
        X = x
        Y = y
        CLICKED = True
        # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        None
    if event == cv2.EVENT_RBUTTONDOWN:
        X = x
        Y = y
        R_CLICKED = True
        '''
    List of possible values for event :
    EVENT_MOUSEMOVE
    EVENT_LBUTTONDOWN
    EVENT_RBUTTONDOWN
    EVENT_MBUTTONDOWN
    EVENT_LBUTTONUP
    EVENT_RBUTTONUP
    EVENT_MBUTTONUP
    EVENT_LBUTTONDBLCLK
    EVENT_RBUTTONDBLCLK
    EVENT_MBUTTONDBLCLK

    List of possible values for flags :    
    EVENT_FLAG_LBUTTON
    EVENT_FLAG_RBUTTON
    EVENT_FLAG_MBUTTON
    EVENT_FLAG_CTRLKEY
    EVENT_FLAG_SHIFTKEY
    EVENT_FLAG_ALTKEY
    '''

def nothing(x) :
    pass


def detect_aruco(frame) :
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_100)
    parameters =  aruco.DetectorParameters_create()
    
    corners, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
    frame = aruco.drawDetectedMarkers(frame, corners)
    return corners, ids

def corners2xyt(corners, ids) :
    """
    Takes a list of corners and a list of ids (sharing the same length, typically the output of detect_aruco) and outputs a dictionnary {id:[x, y, theta]}. theta is in degrees.
    """
    index = -1
    result = {}
    if (ids == None or len(ids) == 0) :
        return None
    for id in ids :
        index = index + 1
        c = corners[index][0] #wtf. Todo, check out the actual format
        id = id[0] #wtf
        tl = c[0]
        tr = c[1]
        br = c[2]
        bl = c[3]
        x = (tl[0] + br[0])/2.0
        y = (tl[1] + br[1])/2.0
        print("id = " + str(id))
        midle_point = ((tl[0] + tr[0])/2.0, (tl[1] + tr[1])/2.0)
        theta = math.atan2(midle_point[1]-y,midle_point[0]-x)*RAD2DEG
        result[id] = [x, y, theta]
    return result


def color_test() :
    global X, Y, CLICKED

    cap = cv2.VideoCapture(CAM_ID)
    list_of_windows = ["frame", "hsv", "output", "output_eroded", "output_eroded_dilated"]
    delta_x = 640
    delta_y = 480
    index = -1
    for w in list_of_windows :
        index = index + 1
        y_index = index/3
        x_index = index%3
        cv2.namedWindow(w)
        cv2.moveWindow(w, x_index*delta_x, y_index*delta_y)

    cv2.setMouseCallback("hsv", mouse_action)

    kernel_size = 3
    # create trackbars for color change
    cv2.createTrackbar('minY','output',0,255,nothing)
    cv2.createTrackbar('maxY','output',0,255,nothing)
    cv2.createTrackbar('minU','output',0,255,nothing)
    cv2.createTrackbar('maxU','output',0,255,nothing)
    cv2.createTrackbar('minV','output',0,255,nothing)
    cv2.createTrackbar('maxV','output',0,255,nothing)
    cv2.createTrackbar('kernel_erode','output',3,15,nothing)
    cv2.createTrackbar('kernel_dilate','output',3,15,nothing)
    t_old = time.time()
    t = t_old
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        if (not(ret)) :
            print("Frame capture failed !")
            continue
        
        # Convert BGR to HSV
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        minY = cv2.getTrackbarPos('minY','output')
        maxY = cv2.getTrackbarPos('maxY','output')
        minU = cv2.getTrackbarPos('minU','output')
        maxU = cv2.getTrackbarPos('maxU','output')
        minV = cv2.getTrackbarPos('minV','output')
        maxV = cv2.getTrackbarPos('maxV','output')
        kernel_erode_size = cv2.getTrackbarPos('kernel_erode','output')
        kernel_dilate_size = cv2.getTrackbarPos('kernel_dilate','output')
        
        lowerBound = np.array([minY, minU, minV]);
        upperBound = np.array([maxY, maxU, maxV]);
        
        output = cv2.inRange(hsv, lowerBound, upperBound);

        kernel_erode = np.ones((kernel_erode_size, kernel_erode_size), np.uint8)
        kernel_dilate = np.ones((kernel_dilate_size, kernel_dilate_size), np.uint8)

        output_eroded = cv2.erode(output, kernel_erode)
        output_eroded_dilated = cv2.erode(output_eroded, kernel_dilate)
        t = time.time()
        freq = 1.0/(t - t_old)
        t_old = t
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        cv2.imshow('frame',frame)
        cv2.imshow('hsv', hsv)
        cv2.imshow('output', output)
        cv2.imshow('output_eroded', output_eroded)
        cv2.imshow('output_eroded_dilated', output_eroded_dilated)

        if CLICKED == True :
            pixel = hsv[Y, X] # Row first then column !!!
            print("[x " + str(X) + ", y " + str(Y) + "] = " + str(pixel))
            CLICKED = False

        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
    sys.exit()

def color_calib() :
    global X, Y, CLICKED, R_CLICKED

    cap = cv2.VideoCapture(CAM_ID)
    list_of_windows = ["frame", "hsv"]
    delta_x = 640
    delta_y = 480
    index = -1
    for w in list_of_windows :
        index = index + 1
        y_index = index/3
        x_index = index%3
        cv2.namedWindow(w)
        cv2.moveWindow(w, x_index*delta_x, y_index*delta_y)

    cv2.setMouseCallback("hsv", mouse_action)

    t_old = time.time()
    t = t_old
    list_of_colors = []
    pixels_by_color = {}
    color_index = -1
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue
        # Convert BGR to HSV
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        t = time.time()
        freq = 1.0/(t - t_old)
        t_old = t
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        cv2.imshow('frame',frame)
        cv2.imshow('hsv', hsv)

        if CLICKED == True :
            pixel = hsv[Y, X] # Row first then column !!!
            pixels_by_color[color].append(pixel)
            print("x " + str(X) + ", y " + str(Y) + " = " + str(pixel))
            CLICKED = False
            save_list_of_pixels(pixels_by_color[color], color)
        if R_CLICKED == True :
            if (len(pixels_by_color[color]) > 0) :
                print("Deleting pixel " + str(pixels_by_color[color][-1]) + "for color " + color)
                pixels_by_color[color].pop() #== pop(-1)
                R_CLICKED = False
                save_list_of_pixels[color](pixels_by_color[color], color)

        key = cv2.waitKey(1)
        if (key & 0xFF) == ord('n'):
            color = raw_input("Enter the new color name: ")
            if (not(color in list_of_colors)) :
                list_of_colors.append(color)
                pixels_by_color[color] = []
                color_index = color_index + 1
        if (key & 0xFF) == ord('c'):
            color_index = (color_index + 1)%len(list_of_colors)
            color = list_of_colors[color_index]
            print("Current color : " + color)
        if (key & 0xFF) == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
    sys.exit()

def main_color() :
    global X, Y, CLICKED, t, goal_x, goal_y
    t0 = time.time()
    #m = MetabotV2('/dev/rfcomm2')
    #m.start()
    cap = cv2.VideoCapture(CAM_ID)
    cv2.namedWindow("frame")
    cv2.namedWindow("hsv")
    cv2.namedWindow("blue")
    cv2.setMouseCallback("hsv", mouse_action)

    #lowerBound = np.array([120, 170, 180]);
    #upperBound = np.array([182, 195, 195]);

    lowerBound = np.array([0, 153, 200]);
    upperBound = np.array([200, 255, 255]);
    kernel_size = 3
    # create trackbars for color change
    cv2.createTrackbar('minY','frame',0,255,nothing)
    cv2.createTrackbar('maxY','frame',0,255,nothing)
    cv2.createTrackbar('minU','frame',0,255,nothing)
    cv2.createTrackbar('maxU','frame',0,255,nothing)
    cv2.createTrackbar('minV','frame',0,255,nothing)
    cv2.createTrackbar('maxV','frame',0,255,nothing)
    cv2.createTrackbar('kernel_size','frame',1,15,nothing)

    new_cx, cx = 0, 0
    new_cy, cy = 0, 0
    
    while(True):
        t = time.time() - t0
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue
        
        # Convert BGR to HSV
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        # Our operations on the frame come here
        #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if CLICKED == True :
            pixel = frame[X, Y]
            print("[x " + str(X) + ", y] " + str(Y) + " = " + str(pixel))
            CLICKED = False


        minY = cv2.getTrackbarPos('minY','frame')
        maxY = cv2.getTrackbarPos('maxY','frame')
        minU = cv2.getTrackbarPos('minU','frame')
        maxU = cv2.getTrackbarPos('maxU','frame')
        minV = cv2.getTrackbarPos('minV','frame')
        maxV = cv2.getTrackbarPos('maxV','frame')
        #kernel_size = cv2.getTrackbarPos('kernel_size','frame')
        
        #lowerBound = np.array([minY, minU, minV]);
        #upperBound = np.array([maxY, maxU, maxV]);
        
        blue = cv2.inRange(hsv, lowerBound, upperBound);

        kernel = np.ones((kernel_size, kernel_size), np.uint8)
        big_kernel = np.ones((2*kernel_size, 2*kernel_size), np.uint8)

        blue = cv2.erode(blue, kernel)

        
        # Dirty blob selection and centroid.
        contours,hierarchy = cv2.findContours(blue, 1, 2)
        # Taking the biggest countour
        maxLength = 0
        maxId = 0
        id = -1
        for c in contours :
            id = id + 1
            if (len(contours) > maxLength) :
                maxLength = len(contours)
                maxId = id

        if id == -1 :
            break
        print("Max id = " + str(maxId) + ", length = " + str(maxLength))
        contour = contours[id]
        # CoM
        try :
            M = cv2.moments(contour)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            new_cx = cx
            new_cy = cy
        except :
            print("Woops !")

        cv2.circle(frame, (new_cx, new_cy), 10, (200, 0, 200), -1)
        cv2.circle(frame, (int(goal_x), int(goal_y)), 10, (200, 0, 0), -1)

        #dx, dy, dtheta = tick_asserv(t, cx, cy)
        #m.control(dx, dy, dtheta)
        
        cv2.imshow('frame',frame)
        cv2.imshow('blue', blue)
        cv2.imshow('hsv', hsv)

        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    # When everything done, release the capture
    m.stop()
    cap.release()
    cv2.destroyAllWindows()
    sys.exit()


def init_asserv() :
    max_d = 200 # mm/s
    max_a = 60 # deg/s
    PID_d = pid.PID(P=3.0, I=0.1, D=0.0, Derivator=0, Integrator=0, Integrator_max=max_d)
    PID_a = pid.PID(P=2.0, I=0.1, D=0.0, Derivator=0, Integrator=0, Integrator_max=max_a)
    ass = asserv.Asserv(PID_d, PID_a, max_d, max_a)

    return ass

def main_aruco(rfcomm) :
    global X, Y, CLICKED, t
    t0 = time.time()
    t_old = 0
    t = 0
    if (WITH_ROBOT) :
        m = MetabotV2(rfcomm) #'/dev/rfcomm2'
        m.start()
        m2 = MetabotV2('/dev/rfcomm3')
        m2.start()
        cap = cv2.VideoCapture(CAM_ID)
        cv2.namedWindow("frame")

    asserv = init_asserv()
    
    cx, cy, ctheta, cx2, cy2, ctheta2 = 0, 0, 0, 0, 0, 0
    p_distance = 40
    start_x = 640/2
    start_y = 480/2
    fake_x = start_x
    fake_y = start_y
    fake_theta = 0
    mm2px = 1

    asserv = init_asserv()
    
    while(True):
        t = time.time() - t0
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue
        
        if CLICKED == True :
            pixel = frame[X, Y]
            print("[x " + str(X) + ", y] " + str(Y) + " = " + str(pixel))
            CLICKED = False

        corners, ids = detect_aruco(frame)
        detected = corners2xyt(corners, ids)
        print("Detected = " + str(detected))
        if WITH_ROBOT :
            if (detected != None and WHITE_ID in detected) :
                cx, cy, ctheta = detected[WHITE_ID]
                arrow_point = (int(cx + p_distance*math.cos(ctheta*DEG2RAD)), int(cy + p_distance*math.sin(ctheta*DEG2RAD)))
                # The actual position of the robot is represented as an arrow
                cv2.arrowedLine(frame, (int(cx), int(cy)), arrow_point, (255, 255, 255), 4)
            if (detected != None and VIOLET_ID in detected) :
                cx2, cy2, ctheta2 = detected[VIOLET_ID]
                arrow_point2 = (int(cx2 + p_distance*math.cos(ctheta2*DEG2RAD)), int(cy2 + p_distance*math.sin(ctheta2*DEG2RAD)))
                # The actual position of the robot is represented as an arrow
                cv2.arrowedLine(frame, (int(cx2), int(cy2)), arrow_point2, (211, 0, 148), 4)

        # And oriented circle
        dt = t - t_old
        t_old = t
        goal_x = 160*math.sin(2*math.pi*0.1*t) + start_x
        goal_y = 160*math.cos(2*math.pi*0.1*t) + start_y
        goal_theta = RAD2DEG*math.atan2(goal_y - start_y, goal_x - start_x)
        dx, dy, dtheta = asserv.tick_asserv(cx, cy, ctheta, goal_x, goal_y, goal_theta)
        #Second robot
        goal_x2 = 160*math.sin(2*math.pi*0.1*t + math.pi) + start_x
        goal_y2 = 160*math.cos(2*math.pi*0.1*t + math.pi) + start_y
        goal_theta2 = RAD2DEG*math.atan2(goal_y - start_y, goal_x - start_x) + 180
        dx2, dy2, dtheta2 = asserv.tick_asserv(cx2, cy2, ctheta2, goal_x2, goal_y2, goal_theta2)
        
        if WITH_ROBOT :
            m.control(dx, dy, dtheta)
            m2.control(dx2, dy2, dtheta2)

        # Simple simulator of the robot
        fake_dx, fake_dy, fake_dtheta = asserv.tick_asserv(fake_x, fake_y, fake_theta, goal_x, goal_y, goal_theta)
        fake_x = fake_x + fake_dx*mm2px*dt*math.cos(fake_theta*DEG2RAD) - fake_dy*mm2px*dt*math.sin(fake_theta*DEG2RAD)
        fake_y = fake_y + fake_dy*mm2px*dt*math.cos(fake_theta*DEG2RAD) + fake_dx*mm2px*dt*math.sin(fake_theta*DEG2RAD)
        fake_theta = fake_theta + fake_dtheta*dt
        
        goal_arrow_point = (int(goal_x + p_distance*math.cos(goal_theta*DEG2RAD)), int(goal_y + p_distance*math.sin(goal_theta*DEG2RAD)))
        # The goal position of the robot is represented as an arrow
        cv2.arrowedLine(frame, (int(goal_x), int(goal_y)), goal_arrow_point, (200, 0, 0), 4)
        
        #Fake robot
        fake_arrow_point = (int(fake_x + p_distance*math.cos(fake_theta*DEG2RAD)), int(fake_y + p_distance*math.sin(fake_theta*DEG2RAD)))
        print("dx = " + str(dx) + ", dy = " + str(dy))
        print("fake_x = " + str(fake_x) + ", fake_y = " + str(fake_y))
        cv2.arrowedLine(frame, (int(fake_x), int(fake_y)), fake_arrow_point, (0, 0, 0), 4)
        cv2.imshow('frame',frame)

        freq = 1.0/dt
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    # When everything done, release the capture
    if WITH_ROBOT :
        m.stop()
        m2.stop()
        cap.release()
        cv2.destroyAllWindows()
        sys.exit()


def main(rfcomms) :
    global X, Y, CLICKED, t
    t0 = time.time()
    t_old = 0
    t = 0
    robots = []

    cap = cv2.VideoCapture(CAM_ID)
    cv2.namedWindow("frame")

    asserv = init_asserv()
    if (WITH_ROBOT) :
        # Creating a robot for each rfcomm port
        index = -1
        for rf in rfcomms :
            index = index + 1
            name, aruco_id, color = ROBOT_IDS[index]
            r = robot.Robot(name, rf, aruco_id, 0, 0, 0, asserv, color)
            robots.append(r)
        for r in robots :
            r.m.start()
            time.sleep(2.0)

    start_x = 640/2
    start_y = 480/2
    fake_x = start_x
    fake_y = start_y
    fake_theta = 0
    mm2px = 1

    asserv = init_asserv()
    
    while(True):
        t = time.time() - t0
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue
        
        if CLICKED == True :
            pixel = frame[X, Y]
            print("[x " + str(X) + ", y] " + str(Y) + " = " + str(pixel))
            CLICKED = False

        corners, ids = detect_aruco(frame)
        detected = corners2xyt(corners, ids)
        print("Detected = " + str(detected))
        if WITH_ROBOT :
            #Updating the current positions of the robots
            for r in robots :
                r.check_detected(detected)
                
        # And oriented circle
        dt = t - t_old
        t_old = t
        index = -1

        for r in robots :
            angle_sep = math.pi*2/float(len(robots))
            index = index + 1
            goal_x = 140*math.sin(2*math.pi*0.08*t + index*angle_sep) + start_x
            goal_y = 140*math.cos(2*math.pi*0.08*t + index*angle_sep) + start_y
            goal_theta = RAD2DEG*math.atan2(goal_y - start_y, goal_x - start_x) + index*angle_sep*RAD2DEG
            r.tick_asserv()
            r.update_goal(goal_x, goal_y, goal_theta)
            
        if WITH_ROBOT :
            for r in robots :
                r.show_on_image(frame)
                r.send_orders()
                
        """
        # Simple simulator of the robot
        fake_dx, fake_dy, fake_dtheta = asserv.tick_asserv(fake_x, fake_y, fake_theta, goal_x, goal_y, goal_theta)
        fake_x = fake_x + fake_dx*mm2px*dt*math.cos(fake_theta*DEG2RAD) - fake_dy*mm2px*dt*math.sin(fake_theta*DEG2RAD)
        fake_y = fake_y + fake_dy*mm2px*dt*math.cos(fake_theta*DEG2RAD) + fake_dx*mm2px*dt*math.sin(fake_theta*DEG2RAD)
        fake_theta = fake_theta + fake_dtheta*dt
        
        goal_arrow_point = (int(goal_x + p_distance*math.cos(goal_theta*DEG2RAD)), int(goal_y + p_distance*math.sin(goal_theta*DEG2RAD)))
        # The goal position of the robot is represented as an arrow
        cv2.arrowedLine(frame, (int(goal_x), int(goal_y)), goal_arrow_point, (200, 0, 0), 4)
        
        #Fake robot
        fake_arrow_point = (int(fake_x + p_distance*math.cos(fake_theta*DEG2RAD)), int(fake_y + p_distance*math.sin(fake_theta*DEG2RAD)))
        print("dx = " + str(dx) + ", dy = " + str(dy))
        print("fake_x = " + str(fake_x) + ", fake_y = " + str(fake_y))
        cv2.arrowedLine(frame, (int(fake_x), int(fake_y)), fake_arrow_point, (0, 0, 0), 4)
        """

        freq = 1.0/dt
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        cv2.imshow('frame',frame)
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    # When everything done, release the capture
    if WITH_ROBOT :
        for r in robots :
            r.m.stop()

    cap.release()
    cv2.destroyAllWindows()
    sys.exit()


def gravitational(rfcomms, mode=0) :
    global X, Y, CLICKED, t
    t0 = time.time()
    t_old = 0
    t = 0
    robots = []

    cap = cv2.VideoCapture(CAM_ID)
    cv2.namedWindow("frame")

    asserv = init_asserv()
    if (WITH_ROBOT) :
        # Creating a fake robot that will be the sun (we will detect it but not control it)
        name, aruco_id, color = ROBOT_IDS[-1]
        sun = robot.Robot(name, -1, aruco_id, 0, 0, 0, asserv, color, fake=True)
        # Creating a robot for each rfcomm port
        index = -1
        for rf in rfcomms :
            index = index + 1
            name, aruco_id, color = ROBOT_IDS[index]
            r = robot.Robot(name, rf, aruco_id, 0, 0, 0, asserv, color)
            robots.append(r)
        for r in robots :
            r.m.start()
            time.sleep(2.0)

    start_x = 640/2
    start_y = 480/2
    fake_x = start_x
    fake_y = start_y
    fake_theta = 0
    mm2px = 1

    asserv = init_asserv()
    
    while(True):
        t = time.time() - t0
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue
        
        if CLICKED == True :
            pixel = frame[X, Y]
            print("[x " + str(X) + ", y] " + str(Y) + " = " + str(pixel))
            CLICKED = False

        corners, ids = detect_aruco(frame)
        detected = corners2xyt(corners, ids)
        print("Detected = " + str(detected))
        if WITH_ROBOT :
            #Updating the current positions of the robots
            sun.check_detected(detected)
            for r in robots :
                r.check_detected(detected)
                
        # A circle around the sun
        dt = t - t_old
        t_old = t
        index = -1
        """
        min_radius = 130
        max_radius = 200
        if (sun.theta < 90 and sun.theta > -90) :
            radius = min_radius + ((max_radius - min_radius)/180.0)*(sun.theta + 90)
            mode = 0
        else :
            #Going back to the [-90, 90] range
            fake_theta = np.sign(sun.theta)*180 - sun.theta
            radius = min_radius + ((max_radius - min_radius)/180.0)*(fake_theta + 90)
            mode = 1
        """
        radius = 140
        for r in robots :
            angle_sep = math.pi*2/float(len(robots))
            index = index + 1
             if mode == 0 :
                 goal_x = radius*math.sin(2*math.pi*0.08*t + index*angle_sep) + sun.x
                 goal_y = radius*math.cos(2*math.pi*0.08*t + index*angle_sep) + sun.y
                 goal_theta = RAD2DEG*math.atan2(goal_y - sun.y, goal_x - sun.x) + 180
             else :
                 goal_x = radius*math.sin(index*angle_sep + math.pi/2) + sun.x
                 goal_y = radius*math.cos(index*angle_sep + math.pi/2) + sun.y
                 goal_theta = sun.theta 
                 
             r.tick_asserv()
             r.update_goal(goal_x, goal_y, goal_theta)
             
        if WITH_ROBOT :
            for r in robots :
                r.show_on_image(frame)
                r.send_orders()
                

        freq = 1.0/dt
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        cv2.imshow('frame',frame)
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    # When everything done, release the capture
    if WITH_ROBOT :
        for r in robots :
            r.m.stop()

    cap.release()
    cv2.destroyAllWindows()
    sys.exit()


def tag() :
    t0 = time.time()
    t_old = 0
    t = 0
    robots = []

    cap = cv2.VideoCapture(CAM_ID)
    cv2.namedWindow("frame")
    cv2.namedWindow("persisted")

    persisted_img = np.zeros((480,640,3), np.uint8)
    asserv = init_asserv()
    if (WITH_ROBOT) :
        # Creating a robot for each rfcomm port
        index = -1
        for name, aruco_id, color in ROBOT_IDS :
            r = robot.Robot(name, "", aruco_id, 0, 0, 0, asserv, color, fake=True)
            robots.append(r)
    while(True):
        t = time.time() - t0
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue
        
        corners, ids = detect_aruco(frame)
        detected = corners2xyt(corners, ids)
        for r in robots :
            r.check_detected(detected)

        print("Detected = " + str(detected))

        dt = t - t_old
        t_old = t
        

        for r in robots :
            r.show_on_image(persisted_img, size=40)
            r.show_on_image(frame, size=40)


        freq = 1.0/dt
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        cv2.imshow('frame',frame)
        cv2.imshow('persisted',persisted_img)
        key = cv2.waitKey(1) & 0xFF
        if (key) == ord('r'):
            persisted_img = np.zeros((480,640,3), np.uint8)
        if (key) == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
    sys.exit()

def rectangle(rfcomms) :
    t0 = time.time()
    t_old = 0
    t = 0
    robots = []
    asserv = init_asserv()
    cv2.namedWindow("frame")
    
    if WITH_ROBOT :
        index = -1
        for rf in rfcomms :
            index = index + 1
            name, aruco_id, color = ROBOT_IDS[index]
            r = robot.Robot(name, rf, aruco_id, 0, 0, 0, asserv, color)
            robots.append(r)
        for r in robots :
            r.m.start()
            time.sleep(2.0)
            
    while(True):
        t = time.time() - t0
        dt = t - t_old
        t_old = t
        goal_theta = 90*math.sin(math.pi*2*0.1*t)
        for r in robots :
            r.dx = 0
            r.dy = 0
            r.dtheta = goal_theta
            
        if WITH_ROBOT :
            for r in robots :
                r.send_orders()

        key = cv2.waitKey(1) & 0xFF
        if (key) == ord('q'):
            # When everything's done, release the capture
            if WITH_ROBOT :
                for r in robots :
                    r.m.stop()
            break
        
    cv2.destroyAllWindows()
    sys.exit()

    
if __name__ == '__main__':
    if(len(sys.argv) == 2 and sys.argv[1] == "-c") :
        print("Color calib. Press n to add a new color, c to cycle between your colors, L click to add a pixel to a color and R click to delete the last pixel of the current color")
        color_calib()
    elif(len(sys.argv) == 4 and sys.argv[1] == "-l") :
        filename = sys.argv[2]
        percentage = float(sys.argv[3])
        get_boundaries_for_color(filename, percentage)
    elif(len(sys.argv) == 2 and sys.argv[1] == "-t") :
        color_test()
    elif(len(sys.argv) == 3 and sys.argv[1] == "-a") :
        main_aruco(sys.argv[2])
    elif(len(sys.argv)  > 2 and sys.argv[1] == "-m") :
        list_of_rfcoms = []
        for i in range(2, len(sys.argv)) :
            list_of_rfcoms.append(sys.argv[i])
            print(list_of_rfcoms)
            time.sleep(1)
            main(list_of_rfcoms)
    elif(len(sys.argv)  > 2 and sys.argv[1] == "-g") :
        list_of_rfcoms = []
        for i in range(2, len(sys.argv)) :
            list_of_rfcoms.append(sys.argv[i])
            print(list_of_rfcoms)
            time.sleep(1)
            gravitational(list_of_rfcoms)
    elif(len(sys.argv) == 2 and sys.argv[1] == "-f") :
        tag()
    elif(len(sys.argv)  > 2 and sys.argv[1] == "-test") :
        list_of_rfcoms = []
        for i in range(2, len(sys.argv)) :
            list_of_rfcoms.append(sys.argv[i])
            print(list_of_rfcoms)
            time.sleep(1)
            rectangle(list_of_rfcoms)
    else :
        print("Usage:")
        print("main.py -c -> to calibrate the colors")
        print("main.py -l filename percentage -> loads a list of pixels and does stuff with it (percentage [0, 100])")
        print("main.py -t -> to fool around")
        print("main.py -m -> to launch the metabot control test")

