import time, math, sys
import cv2
from metabot import MetabotV2
import asserv
import os.path


DEG2RAD = math.pi/180.0
RAD2DEG = 180.0/math.pi

class Robot() :
    """
    Represents a tracked robot
    """
    def __init__(self, name, rfcom, aruco_id, x, y, theta, asserv, color, fake=False) :
        self.name = name
        self.rfcom = rfcom
        self.aruco_id = aruco_id
        self.x = x
        self.y = y
        self.theta = theta
        self.asserv = asserv
        self.color = color
        self.goal_x = x
        self.goal_y = y
        self.goal_theta = theta
        self.is_detected = False
        self.dx = 0
        self.dy = 0
        self.dtheta = 0
        self.dx_p = 0
        self.dy_p = 0
        self.dtheta_p = 0
        self.led_color = "yellow"
        self.send_color = True
        self.stop = False
        self.predator_x = 0
        self.predator_y = 0
        if not(fake) :
            print("Trying to create an instance of MetabotV2 on the port '" + str(rfcom) + "' for robot '" + name + "'")
        self.m = MetabotV2(rfcom)
            #            if (os.path.isfile(rfcom)) :
                 
    def check_detected(self, detected_dict) :
        if (detected_dict != None and self.aruco_id in detected_dict):
            self.x, self.y, self.theta = detected_dict[self.aruco_id]
            self.is_detected = True
        else :
            self.is_detected = False

    def show_on_image(self, img, size=40) :
        #Current position
        arrow_point = (int(self.x + size*math.cos(self.theta*DEG2RAD)), int(self.y + size*math.sin(self.theta*DEG2RAD)))
        # The actual position of the robot is represented as an arrow
        cv2.arrowedLine(img, (int(self.x), int(self.y)), arrow_point, self.color, 4)
        
        #Current goal
        arrow_point = (int(self.goal_x + size*math.cos(self.goal_theta*DEG2RAD)), int(self.goal_y + size*math.sin(self.goal_theta*DEG2RAD)))
        color = (self.color[0]/2, self.color[1]/2, self.color[2]/2)
        cv2.arrowedLine(img, (int(self.goal_x), int(self.goal_y)), arrow_point, color, 4)

    def update_goal(self, x, y, theta) :
        self.goal_x = x
        self.goal_y = y
        self.goal_theta = theta
        
    def tick_asserv(self) :
        self.dx, self.dy, self.dtheta = self.asserv.tick_asserv(self.x, self.y, self.theta, self.goal_x, self.goal_y, self.goal_theta)
        

    def send_orders(self) :
        if (self.stop) :
            self.m.control(0, 0, 0)
        else :
            self.m.control(self.dx, self.dy, self.dtheta)
        if (self.send_color) :
            self.m.setLeds(self.led_color)

    def control_smooth(self, dx, dy, dtheta, predator=False):
        factor = 10
        self.dx_p = int((self.dx_p*factor + dx)/float(factor + 1))
        self.dy_p = int((self.dy_p*factor + dy)/float(factor + 1))
        self.dtheta_p = int((self.dtheta_p*factor + dtheta)/float(factor + 1)) 

        if (self.stop) :
            dx = 0
            dy = 0
            dtheta = 0

        # Fish algorithm, not sure if appropriate
        if (predator) :
            # Trying to avoid the predator
            fear = 1
            radius = 90
            max_radius = 200
            max_speed = 40
            delta_x = self.predator_x - self.x
            delta_y = self.predator_y - self.y
            distance = math.sqrt(delta_x*delta_x + delta_y*delta_y)
            vector_to_death = [delta_x, delta_y]
            ortho = [-delta_y/distance, delta_x/distance]
            if (distance > radius):
                ortho[0] = fear*ortho[0]*(1/(distance-radius))
                ortho[1] = fear*ortho[1]*(1/(distance-radius))
            else :
                ortho[0] = max_speed*ortho[0]
                ortho[1] = max_speed*ortho[1]
            # TODO chose which side to avoid to
            # Doing a dot product with the [dx, dy] vector to chose the direction
            dot1 = ortho[0]*dx + ortho[1]*dy
            if (dot1 < 0) :
                ortho[0] = -ortho[0]
                ortho[1] = -ortho[1] 
            max_coord = max(ortho[0], ortho[1])
            if (max_coord > max_speed) :
                factor = max_speed/float(max_coord)
                ortho[0] = ortho[0]*factor
                ortho[1] = ortho[1]*factor
            #Now we know where we want to go in the world, let's go local
            dx, dy, dtheta = self.asserv.tick_asserv(self.x, self.y, self.theta, self.x + ortho[0], self.y + ortho[1], self.theta)
            self.dx_p = self.dx_p + dx
            self.dy_p = self.dy_p + dy 
        
        '''
        # Simple vectorial approach
        if (predator) :
            # Trying to avoid the predator
            radius = 90
            max_radius = 200
            max_speed = 40
            delta_x = self.predator_x - self.x
            delta_y = self.predator_y - self.y
            distance = math.sqrt(delta_x*delta_x + delta_y*delta_y)
            ortho = [-delta_x/distance, -delta_y/distance]
            print("Distance = " + str(distance))
            if (distance < radius):
                ortho[0] = max_speed*ortho[0]
                ortho[1] = max_speed*ortho[1]
            elif (distance > max_radius):
                ortho[0] = 0
                ortho[1] = 0
            else:
                ortho[0] = max_speed*ortho[0]*((distance - radius)/(max_radius - radius))
                ortho[1] = max_speed*ortho[1]*((distance - radius)/(max_radius - radius))
            print("Pre nerf : " + str(ortho[0]) + " ," + str(ortho[1]))
            max_coord = max(abs(ortho[0]), abs(ortho[1]))
            if (max_coord > max_speed) :
                factor = max_speed/float(max_coord)
                ortho[0] = ortho[0]*factor
                ortho[1] = ortho[1]*factor

            #Now we know where we want to go in the world, let's go local
            dx, dy, dtheta = self.asserv.tick_asserv(self.x, self.y, self.theta, self.x + ortho[0], self.y + ortho[1], self.theta)
            print("dx = " + str(dx) + ", dy = " + str(dy))
            self.dx_p = self.dx_p + dx
            self.dy_p = self.dy_p + dy 
        '''
        self.m.control(self.dx_p, self.dy_p, self.dtheta_p)
        if (self.send_color) :
            self.m.setLeds(self.led_color)

    def reset(self) :
        self.m.stop()
        time.sleep(1)
        self.m.start()
    
    def update_predator(self, pos) :
        self.predator_x = pos[0]
        self.predator_y = pos[1]

    # How does a relative order transform into an absolute one?
    def relative2absolute_order(self, dx, dy, dtheta) :
        # Projecting dx and dy to X
        self.goal_x = self.x + dx*math.cos(DEG2RAD*self.theta) - dy*math.sin(DEG2RAD*self.theta)
        self.goal_y = self.y + dx*math.sin(DEG2RAD*self.theta) - dy*math.cos(DEG2RAD*self.theta)
        self.goal_theta = self.theta + dtheta
        
