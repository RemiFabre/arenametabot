import math
import pid
import numpy as np

DEG2RAD = math.pi/180.0
RAD2DEG = 180.0/math.pi

def angle_diff(a, b) :
    '''
    Returns the smallest signed angle between 2 angles
    '''
    delta = b - a
    delta = (delta + 180) % 360 - 180
    return delta


class Asserv :
    def __init__(self, PID_d, PID_a, max_d, max_a):
        # PIDs for distance (x, y) and angle
        self.PID_d = PID_d
        self.PID_a = PID_a
        self.max_d = max_d
        self.max_a = max_a

    def tick_asserv(self, x, y, theta, goal_x, goal_y, goal_theta) :
        """
        theta in degrees
        """
#        k_p = 1
#        k_p_a = 3

        #Projecting the x and y axis of the robot on the line ((x, y);(goal_x, goal_y)). alpha is the angle between that line andd the robot x axis.
        delta_x  = goal_x - x
        delta_y  = -(goal_y - y)
        distance = math.sqrt(delta_x*delta_x + delta_y*delta_y)
        alpha = math.atan2(delta_y, delta_x) + theta*DEG2RAD
        self.PID_d.target = distance
        distance = self.PID_d.update(0)
        #distance = distance*k_p

        #Making sure the max value is not broken
        if (abs(alpha%(2*math.pi)) == math.pi/2) :
            cond1 = self.max_d
        else :
            cond1 = abs(self.max_d/math.cos(alpha))
        if (abs(alpha%(2*math.pi)) == 0) :
            cond2 = self.max_d
        else:
            cond2 = abs(self.max_d/math.sin(alpha))
        max_distance = min(cond1, cond2)
        distance = min(distance, max_distance)

        value_x = distance*math.cos(alpha)
        value_y = distance*math.sin(alpha)

        # Handling the cyclic nature of the rotation here
        delta_angle = angle_diff(theta, goal_theta)
        self.PID_a.target = delta_angle
        value_theta = self.PID_a.update(0)
#        value_theta = k_p_a*delta_angle
        if (abs(value_theta) > self.max_a) :
            value_theta = self.max_a*np.sign(value_theta)

        #print (value_x, value_y, value_theta)
        return value_x, -value_y, value_theta
