import time, math, random, sys, os, datetime
import csv
import numpy as np
import cv2
import cv2.aruco as aruco
from metabot import MetabotV2
import pid
import asserv
import robot
from aruco_utils import *
import traceback

'''
ROBOT_IDS = [
    ("White", 3, (255, 255, 255), "/dev/rfcomm4"),
    ("MajinBuu", 2, (147,20,255), "/dev/rfcomm5"),
    ("Yellow", 1, (0, 255, 255), "/dev/rfcomm6"),
    ("Lavande", 4, (211, 0, 148), "/dev/rfcomm7"),
    ("Marron", 5, (88, 41, 0), "/dev/rfcommx"),
    ("Marker", 12, (0, 0, 0), "NA")]
'''
ROBOT_IDS = [
    ("Yellow", 1, (0, 255, 255), "/dev/rfcomm5")
]

#print(cv2.__version__)

PREDATOR = 2
MARKER = 12
CAM_ID = 2
CAM_ID_CUBE = 0
DEG2RAD = math.pi/180.0
RAD2DEG = 180.0/math.pi

X = 0
Y = 0

CENTER = [62, 245]
CLICKED = False
R_CLICKED = False
now = datetime.datetime.now().strftime("%Y-%m-%d_%H_%M_%S")
WITH_ROBOT = True
offset = 40
xlim = [offset, 640 - offset]
ylim = [offset, 480 - offset]

#Variables initialization (TODO make this clean some day)
asserv = init_asserv()
robots = []

def save_checkpoints(list_of_checkpoints, filename) :
    # Erasing and rewrinting everything.

    with open(filename, 'w') as csvfile:
        str_delimiter = " "
        data_writer = csv.writer(csvfile, delimiter=str_delimiter)
        for c in list_of_checkpoints :
            data_writer.writerow(c)

def load_checkpoints(filename) :
    try :
        with open(filename, 'r') as csvfile:
            rows = csv.reader(csvfile, delimiter=' ', quotechar='|')
            list_of_checkpoints = []
            for r in rows :
                list_of_checkpoints.append((int(r[0]), int(r[1])))
            return list_of_checkpoints
    except Exception :
        print("Failed to read : '", filename, "'")
        return

def mouse_action(event, x, y, flags, param):
    global X, Y, CLICKED, R_CLICKED
    if event == cv2.EVENT_LBUTTONDOWN:
        X = x
        Y = y
        CLICKED = True
    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        None
    if event == cv2.EVENT_RBUTTONDOWN:
        X = x
        Y = y
        R_CLICKED = True
        
def init():
    asserv = init_asserv()
    if (WITH_ROBOT) :
        # Creating a robot instance for each declared robot
        for rob in ROBOT_IDS :
            name, aruco_id, color, rfcomm = rob
            try :
                r = robot.Robot(name, rfcomm, aruco_id, 0, 0, 0, asserv, color)
                robots.append(r)
                print("----------------------> " + str(name) + " is available !")
            except :
                print("Failed to create the robot " + str(name) + ". Most likely, the rfcomm port doesn't exist.")
    print("Variables initialized.")

def click_on_checkpoints(frame):
    global CLICKED
    checkpoints = []
    while(True):
        if CLICKED == True :
            #pixel = frame[Y, X] # Row first then column !!!
            checkpoints.append((X, Y))
            print("Added x=" + str(X) + ", y=" + str(Y))
            CLICKED = False

        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            return checkpoints


def clean_exit():
    for r in robots:
        try:
            r.m.stop()
        except:
            None
    sys.exit()

### Pressing a key ('n') will cycle between activable robots. Pressing another key (' ')
### will select the robot.
def select_robot(nb_robots=1):
    print("Selecting robots...")

    # Just checking if the camera is healthy
    cap = cv2.VideoCapture(CAM_ID)
    cv2.namedWindow("frame")
    # Capture frame-by-frame
    while(True):
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue
        break
    selected = []
    while(len(selected) < nb_robots):
        for r in robots:
            r.m.start()
            r.m.control(0, 0, 0)
            key = cv2.waitKey(0) & 0xFF
            if (key == ord('q')) :
                print("Forced to leave in select robot...")
                clean_exit()
            if (key == ord('n')) :
                r.m.stop()
                continue
            if (key == ord(' ')) :
                selected.append(r)
    return selected

def show_detected():
    cap = cv2.VideoCapture(CAM_ID)
    cv2.namedWindow("frame")
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue

        corners, ids = detect_aruco(frame)
        detected = corners2xyt(corners, ids)
        print("Detected = " + str(detected))
        cv2.imshow('frame',frame)
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            print("Leaving show_detected()...")
            return

### angle_tol=-1 is a code meaning we don't care about the angle
def reached(pos1, pos2, radius=20, angle_tol=10):
    delta_x = pos2[0] - pos1[0]
    delta_y = pos2[1] - pos1[1]
    if (angle_tol == -1):
        delta_a = 0
    else :
        delta_a = pos2[2] - pos1[2]
    distance = math.sqrt(delta_x*delta_x + delta_y*delta_y)
    if ((distance < radius) and ((abs(delta_a) < angle_tol) or angle_tol == -1)) :
        return True
    else :
        return False

### The cube of Aruco markers is used as a controller.
### But, if the orders would make the robot crash into a wall, the orders are not respected.
def get_orders(detected, r, dx, dy, dtheta):
    #Units are *not* in SI (they're not even correct;)
    rotation_speed = 50
    speed = 150
    stop = False
    reset = False
    boost = False
    #print("r.x = " + str(r.x))
    #print("r.y = " + str(r.y))
    #print("r.theta = " + str(r.theta))
    #24 is the center
    if (19 in detected) :
        # Forward
        dx = speed
        dy = 0
        dtheta = 0
    elif (23 in detected) :
        # Backward
        dx = - speed
        dy = 0
        dtheta = 0
    elif (20 in detected) :
        # Positive rotation
        dx = 0
        dy = 0
        dtheta = rotation_speed
    elif (22 in detected) :
        # Negative rotation
        dx = 0
        dy = 0
        dtheta = - rotation_speed
    elif (24 in detected) :
        #Astuce ! (that way we can make a visual difference between speed 0 and a voltage blocked robot)
        dx = -1
        dy = 0
        dtheta = 0
        stop = True
    elif (17 in detected) :
        #Asking for a reset !
        dx = 0
        dy = 0
        dtheta = 0
        stop = False
        reset = True
    elif (21 in detected) :
        #Boost mode!
        boost = True
    else :
        #Maintain orders !
        None
    dx, dy, respected = bound_position(r.x, r.y, dx, dy, speed)

    return dx, dy, dtheta, respected, stop, reset, boost

def get_orders_kid(detected, r, center):
    #Units are *not* in SI (they're not even correct;)
    rotation_speed = 60
    speed = 190
    stop = False
    speed_k = 5
    angle_k = 2
    if (MARKER in detected) :
        x = detected[MARKER][0]
        y = detected[MARKER][1]
        theta = detected[MARKER][2]
        #print("x = " + str(x) + ", y = " + str(y) + ", theta = " + str(theta))
        dx = (x - center[0])*speed_k
        dy = (y - center[1])*speed_k
        dtheta = theta*angle_k
        if (dx > speed):
            dx = speed
        if (dx < -speed):
            dx = -speed
        if (dy > speed):
            dy = speed
        if (dy < -speed):
            dy = -speed
        if (dtheta > rotation_speed):
            dtheta = rotation_speed
        if (dtheta < -rotation_speed):
            dtheta = -rotation_speed
        #print("dx = " + str(dx) + ", dy = " + str(dy) + ", dtheta = " + str(dtheta))
    else :
        dx = 0
        dy = 0
        dtheta = 0
        stop = True

    dx, dy, respected = bound_position(r.x, r.y, dx, dy, speed)

    return dx, dy, dtheta, respected, stop

### Checks if the positions are not out of the physical bound
def bound_position(x, y, dx, dy, speed):
    #TODO debug here (doesn't seem to work when cube controlled)
    global xlim, ylim
    respected = True
    # If the bounds are not respected, the orders are not relative anymore but absolute
    # TODO : make the damn calculation to avoid this weird workaround
    if (x < xlim[0]) :
        dx = x + speed
        dy = y
        respected = False
    if (x > xlim[1]) :
        dx = x - speed
        dy = y
        respected = False
    if (y < ylim[0]) :
        dx = x 
        dy = y + speed
        respected = False
    if (y > ylim[1]) :
        dx = x
        dy = y - speed
        respected = False
    return dx, dy, respected
        
def game(auto_checkpoints=True, auto=False):
    colors = ["white","yellow","green","blue","red","cyan"]
    time_limit = 240
    selected_robots = select_robot(1)
    for r in selected_robots :
        print(r.name + " was selected !")

    cap = cv2.VideoCapture(CAM_ID)
    cap_cube = cv2.VideoCapture(CAM_ID_CUBE)
    cv2.namedWindow("frame")
    cv2.namedWindow("frame_cube")
    cv2.setMouseCallback("frame", mouse_action)

    if (auto_checkpoints == False):
        ret = False
        while(not(ret)):
            print("Failed to read cam")
            ret, frame = cap.read()
        ret = False
        while(not(ret)):
            print("Failed to read cap_cube")
            ret, frame_cube = cap_cube.read()
        
        cv2.imshow('frame',frame)
        cv2.imshow('frame_cube', frame_cube)
        checkpoints = click_on_checkpoints(frame)
        save_checkpoints(checkpoints, "checkpoints.csv")
    checkpoints = load_checkpoints("checkpoints.csv")
    for c in checkpoints:
        print(c)
    
    initial_position = (150, 480/2, 0)
    r = selected_robots[0]
    state = "INIT"
    t0 = time.time()
    t_old = 0
    t = 0
    absolute_orders = True
    next_checkpoint = 0
    dx, dy, dtheta = 0, 0, 0
    boost_ratio = 1.5
    boost_time = 0
    max_boost_time = 1.5
    
    if (auto) :
        # Cleanest code of my life
        state = "AUTO"
    
    print("Starting main loop...")
    while(True) :
        t = time.time() - t0
        reset = False
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture cam " + time.time())
            continue
        ret = False
        ret, frame_cube = cap_cube.read()
        if(not(ret)):
            print("Failed to read cap_cube " + time.time())
            continue
        
        # Aruco marker detection
        corners, ids = detect_aruco(frame)
        detected = corners2xyt(corners, ids)

        corners, ids = detect_aruco(frame_cube)
        detected2 = corners2xyt(corners, ids)
        detected.update(detected2)

        #print("Detected = " + str(detected))
        if WITH_ROBOT :
            #Updating the current position of the robot
            r.check_detected(detected)
        dt = t - t_old
        t_old = t

        # State machine management
        if (state == "INIT") :
            #Going to initial position
            absolute_orders = True
            goal_x = initial_position[0]
            goal_y = initial_position[1]
            goal_theta = initial_position[2]
            if (reached(initial_position, (r.x, r.y, r.theta), radius=20, angle_tol=10)):
                print("Going to STEADY")
                state = "STEADY"
                print("Press space to start a new game !")
        if (state == "STEADY") :
            dx, dy, dtheta, respected, stop, reset, boost = get_orders(detected, r, dx, dy, dtheta)
            key = cv2.waitKey(1) & 0xFF
            if (key == ord(' ') or (dx==-1)) :
                # dx == -1 only happens when a specific face of the cube is shown...
                print("Going to PLAY")
                state = "PLAY"
                t0 = time.time()
                t_old = 0
                t = 0
        if (state == "PLAY") :
            # Updating the orders for the robot
            absolute_orders = False
            if (MARKER in detected) :
                # Kiddo version of the control
                dx, dy, dtheta, respected, stop = get_orders_kid(detected, r, CENTER)
            else :
                dx, dy, dtheta, respected, stop, reset, boost = get_orders(detected, r, dx, dy, dtheta)
            if (stop) :
                r.stop = True
            else :
                r.stop = False
            if (respected == False) :
                # The time is spent twice as fast if a boundary is too close !
                t = t + dt
                t_old = t_old + dt
                r.m.beep(100, 200)
                #print("BORDER WARNING")
                absolute_orders = True
                # Weird workaround...
                goal_x = dx
                goal_y = dy
                goal_theta = r.theta
            if (t > time_limit) :
                r.m.beep(500, 500)
                r.m.setLeds("red")
                time.sleep(0.7)
                r.m.beep(500, 500)
                r.m.setLeds("white")
                time.sleep(0.7)
                r.m.beep(800, 1500)
                r.m.setLeds("red")
                time.sleep(1)
                print("Going to END")
                state = "END"

            #Checking if we hit a checkpoint
            p = checkpoints[next_checkpoint]
            if (reached(p, (r.x, r.y, r.theta), radius=40, angle_tol=-1)):
                # Yey !
                print("Reached checkpoint " + str(next_checkpoint))
                next_checkpoint = next_checkpoint + 1
                r.m.beep(300, 1000)
                #Going to the next color
                r.led_color = colors[next_checkpoint%(len(colors))]
                
                if (next_checkpoint >= len(checkpoints)):
                    #Game is over, gg wp
                    print("Going to END")
                    state = "END"
        if (state == "AUTO") :
            #Going to the next position
            absolute_orders = True
            p = checkpoints[next_checkpoint]
            goal_x = p[0]
            goal_y = p[1]
            goal_theta = 0
            if (reached(p, (r.x, r.y, r.theta), radius=20, angle_tol=-1)):
                # Yey !
                print("Reached checkpoint " + str(next_checkpoint))
                next_checkpoint = (next_checkpoint + 1)%len(checkpoints)
                r.m.beep(300, 1000)
                #Going to the next color
                r.led_color = colors[next_checkpoint%(len(colors))]

        if (state == "END") :
            #Saving time and going back to the begining
            print("time : " + str((t)))
            state = "INIT"
            stop = False
            t0 = time.time()
            t_old = 0
            t = 0
            absolute_orders = True
            next_checkpoint = 0
            r.m.beep(1000, 300)
            time.sleep(0.3)
            r.m.beep(1000, 300)
            time.sleep(0.3)
            r.m.beep(1000, 100)
            time.sleep(0.1)
            r.m.beep(1000, 100)
            time.sleep(0.1)
            r.m.beep(1000, 100)
            time.sleep(0.1)
            print("Going to INIT")
            state = "INIT"
            reset = True
            
        if (absolute_orders) :
            # Giving an absolute X, Y, Theta position
            r.update_goal(goal_x, goal_y, goal_theta)
            r.tick_asserv()
            if (WITH_ROBOT) :
                r.send_orders()
        else :
            # Giving a relative command (dx, dy, dtheta)
            if (WITH_ROBOT) :
                if (boost):
                    if (boost_time > max_boost_time):
                        boost_time = False
                        boost = False    
                    boost_time = boost_time + dt
                r.control_smooth(dx + int(boost)*(boost_ratio - 1)*dx, dy + int(boost)*(boost_ratio - 1)*dy, dtheta + + int(boost)*(boost_ratio - 1)*dtheta)

                #r.m.control(dx, dy, dtheta)
                #Faking an absolute order so the arrow on screen means something
                r.relative2absolute_order(2*dx, 2*dy, 2*dtheta)
                #Trying this out, TODO clean this up
                absolute_orders = True

        r.show_on_image(frame)
        if (r.send_color) :
            r.m.setLeds(r.led_color)


        # Draw limits
        cv2.rectangle(frame, (xlim[0], ylim[0]), (xlim[1], ylim[1]), (0, 0, 255), 10)
        #Draw checkpoints
        for c in checkpoints:
            #point = (int(c[0]), int(c[1]))
            cv2.circle(frame, c, 5, (0, 255, 0), -1) 
        freq = 1.0/dt
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        cv2.putText(frame,"REMAINING TIME : " + "{0:.2f}".format(time_limit - t) + " sec", (0,33), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0,200))
        cv2.putText(frame_cube,"NB CHECKPOINTS : " + str(next_checkpoint), (0,33), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0, 200))
        cv2.imshow('frame',frame)
        cv2.imshow('frame_cube', frame_cube)
        key = cv2.waitKey(1) & 0xFF 
        if (key == ord('q')):
            break
        if (key == ord('r') or reset):
            r.reset()

def trap_trap(auto_checkpoints=True):
    time_limit = 180
    selected_robots = select_robot(1)
    for r in selected_robots :
        print(r.name + " was selected !")

    cap = cv2.VideoCapture(CAM_ID)
    cap_cube = cv2.VideoCapture(CAM_ID_CUBE)
    cv2.namedWindow("frame")
    cv2.namedWindow("frame_cube")
    cv2.setMouseCallback("frame", mouse_action)

    if (auto_checkpoints == False):
        ret = False
        while(not(ret)):
            print("Failed to read cam")
            ret, frame = cap.read()
        ret = False
        while(not(ret)):
            print("Failed to read cap_cube")
            ret, frame_cube = cap_cube.read()
        
        cv2.imshow('frame',frame)
        cv2.imshow('frame_cube', frame_cube)
        checkpoints = click_on_checkpoints(frame)
        
        save_checkpoints(checkpoints, "lines.csv")
    checkpoints = load_checkpoints("lines.csv")
    print("Loaded !")
    for c in checkpoints:
        print(c)
    
    initial_position = (150, 480/2, 0)
    r = selected_robots[0]
    state = "INIT"
    t0 = time.time()
    t_old = 0
    t = 0
    absolute_orders = True
    next_checkpoint = 0
    dx, dy, dtheta = 0, 0, 0
    boost_ratio = 1.5
    boost_time = 0
    max_boost_time = 1.5
    
    print("Starting main loop...")
    while(True) :
        t = time.time() - t0
        reset = False
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture cam " + time.time())
            continue
        ret = False
        ret, frame_cube = cap_cube.read()
        if(not(ret)):
            print("Failed to read cap_cube " + time.time())
            continue
        
        # Aruco marker detection
        corners, ids = detect_aruco(frame)
        detected = corners2xyt(corners, ids)

        corners, ids = detect_aruco(frame_cube)
        detected2 = corners2xyt(corners, ids)
        detected.update(detected2)

        #print("Detected = " + str(detected))
        if WITH_ROBOT :
            #Updating the current position of the robot
            r.check_detected(detected)
        dt = t - t_old
        t_old = t

        if (PREDATOR in detected) :
            r.update_predator(detected[PREDATOR])
        
        # State machine management
        if (state == "INIT") :
            #Going to initial position
            absolute_orders = True
            goal_x = initial_position[0]
            goal_y = initial_position[1]
            goal_theta = initial_position[2]
            if (reached(initial_position, (r.x, r.y, r.theta), radius=20, angle_tol=10)):
                print("Going to STEADY")
                state = "STEADY"
                print("Press space to start a new game !")
        if (state == "STEADY") :
            dx, dy, dtheta, respected, stop, reset, boost = get_orders(detected, r, dx, dy, dtheta)
            key = cv2.waitKey(1) & 0xFF
            if (key == ord(' ') or (dx==-1)) :
                # dx == -1 only happens when a specific face of the cube is shown...
                print("Going to PLAY")
                state = "PLAY"
                t0 = time.time()
                t_old = 0
                t = 0
        if (state == "PLAY") :
            # Updating the orders for the robot
            absolute_orders = False
            if (MARKER in detected) :
                # Kiddo version of the control
                dx, dy, dtheta, respected, stop = get_orders_kid(detected, r, CENTER)
            else :
                dx, dy, dtheta, respected, stop, reset, boost = get_orders(detected, r, dx, dy, dtheta)
            if (stop) :
                r.stop = True
            else :
                r.stop = False
            if (respected == False) :
                # The time is spent twice as fast if a boundary is too close !
                t = t + dt
                t_old = t_old + dt
                r.m.beep(100, 200)
                #print("BORDER WARNING")
                absolute_orders = True
                # Weird workaround...
                goal_x = dx
                goal_y = dy
                goal_theta = r.theta
            if (t > time_limit) :
                r.m.beep(500, 500)
                r.m.setLeds("red")
                time.sleep(0.7)
                r.m.beep(500, 500)
                r.m.setLeds("white")
                time.sleep(0.7)
                r.m.beep(800, 1500)
                r.m.setLeds("red")
                time.sleep(1)
                print("Going to END")
                state = "END"

            #TODO Checking game end condition
            

        if (state == "END") :
            #Saving time and going back to the begining
            print("time : " + str((t)))
            state = "INIT"
            stop = False
            t0 = time.time()
            t_old = 0
            t = 0
            absolute_orders = True
            next_checkpoint = 0
            r.m.beep(1000, 300)
            time.sleep(0.3)
            r.m.beep(1000, 300)
            time.sleep(0.3)
            r.m.beep(1000, 100)
            time.sleep(0.1)
            r.m.beep(1000, 100)
            time.sleep(0.1)
            r.m.beep(1000, 100)
            time.sleep(0.1)
            print("Going to INIT")
            state = "INIT"
            reset = True
            
        if (absolute_orders) :
            # Giving an absolute X, Y, Theta position
            r.update_goal(goal_x, goal_y, goal_theta)
            r.tick_asserv()
            if (WITH_ROBOT) :
                r.send_orders()
        else :
            # Giving a relative command (dx, dy, dtheta)
            if (WITH_ROBOT) :
                if (boost):
                    if (boost_time > max_boost_time):
                        boost_time = False
                        boost = False    
                    boost_time = boost_time + dt
                r.control_smooth(dx + int(boost)*(boost_ratio - 1)*dx, dy + int(boost)*(boost_ratio - 1)*dy, dtheta + + int(boost)*(boost_ratio - 1)*dtheta)
                #r.m.control(dx, dy, dtheta)
                if (r.send_color) :
                    r.m.setLeds(r.led_color)
                #Faking an absolute order so the arrow on screen means something
                r.update_goal(r.x + dx, r.y + dy, r.theta + dtheta)
        r.show_on_image(frame)
        

        # Draw limits
        '''
        for i in range(int(len(checkpoints)/2)):
            p1 = (int(checkpoints[2*i][0]), int(checkpoints[2*i][1]))
            p2 = (int(checkpoints[2*i+1][0]), int(checkpoints[2*i+1][1]))
            cv2.line(frame, p1, p2, (0, 0, 255), 10)
        '''
        freq = 1.0/dt
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        cv2.putText(frame,"REMAINING TIME : " + "{0:.2f}".format(time_limit - t) + " sec", (0,33), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0,200))
        cv2.putText(frame_cube,"NB CHECKPOINTS : " + str(next_checkpoint), (0,33), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0, 200))
        cv2.imshow('frame',frame)
        cv2.imshow('frame_cube', frame_cube)
        key = cv2.waitKey(1) & 0xFF 
        if (key == ord('q')):
            break
        if (key == ord('r') or reset):
            r.reset()
        if (key == ord('a')) :
            state = "AUTO"

if __name__ == '__main__':
    try:
        # Variables initialization
        init()
        
        if(len(sys.argv) > 1 and sys.argv[1] == "-show") :
            show_detected()
        elif(len(sys.argv) > 1 and sys.argv[1] == "-checkpoints") :
            #Clicking on checkpoints
            game(False)
        elif(len(sys.argv) > 1 and sys.argv[1] == "-auto") :
            # Auto mode, for lazy, impressionable people :)
            game(True, True)
        elif(len(sys.argv) == 1) :
            game()
        elif(len(sys.argv) > 2 and sys.argv[1] == "-trap" and sys.argv[1] == "-checkpoints") :
            #Clicking on checkpoints
            trap_trap(False)
        elif(len(sys.argv) > 1 and sys.argv[1] == "-trap") :
            #Clicking on checkpoints
            trap_trap(True)
        else :
            print("To lazy to tell you what to type. Game over.")
    except Exception as e:
        traceback.print_exc()
        #print("Exception : ", e)
    finally:
        clean_exit()
