import cv2
import cv2.aruco as aruco
 
 
'''
    drawMarker(...)
        drawMarker(dictionary, id, sidePixels[, img[, borderBits]]) -> img
'''
 
aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_100)
# second parameter is id number
# last parameter is total image size
index = -1
while True :
    index = index + 1
    img = aruco.drawMarker(aruco_dict, index, 200)
    cv2.imwrite("test_marker.jpg", img)
    cv2.imshow('frame',img)
    cv2.imwrite("./" + str(index) + ".jpg", img)
    key = cv2.waitKey(0)
    if ((key & 0xff) == ord('q')) :
        break
    
cv2.destroyAllWindows()
