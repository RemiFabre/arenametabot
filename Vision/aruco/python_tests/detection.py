import time, math, random, sys, os, datetime
import csv
import numpy as np
import cv2
import cv2.aruco as aruco

CAM_ID = 1
def main() :
    t0 = time.time()
    t_old = t0
    cap = cv2.VideoCapture(CAM_ID)
    cv2.namedWindow("frame")
    
    while(True):
        t = time.time() - t0
        # Capture frame-by-frame
        ret, frame = cap.read()
        if (not(ret)) :
            print("Frame capture failed !")
            continue
        aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_100)
        parameters =  aruco.DetectorParameters_create()

        corners, ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
        #print(corners)
        print ids
        frame = aruco.drawDetectedMarkers(frame, corners)


        freq = 1.0/(t - t_old)
        t_old = t
        cv2.putText(frame,"Freq : " + "{0:.2f}".format(freq) + " Hz", (0,460), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 0, 0))
        cv2.imshow('frame',frame)
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
    sys.exit()


if __name__ == '__main__':
    main()
