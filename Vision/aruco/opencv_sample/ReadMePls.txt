./detect_markers  -d=1 --dp="../detector_params.yml" -l=0.0565

./create_board_charuco -dp="../detector_params.yml" -bb=1 -d=1 -h=7 -w=5 --sl=100 --ml=50 --si=true output.jpg

./calibrate_camera_charuco --ci=1 -d=1 --dp="../detector_params.yml" -h=7 -w=5 --sl=0.0345 --ml=0.01725 -sc=true ../camera_parameters.yml

./detect_board_charuco --ci=1 -d=1 --dp="../detector_params.yml" -h=7 -w=5 --sl=0.0345 --ml=0.01725 -sc=true -c="../camera_parameters.yml"
